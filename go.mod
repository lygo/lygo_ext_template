module bitbucket.org/lygo/lygo_ext_template

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.4
	github.com/cbroglie/mustache v1.1.0
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200619210111-0f592d2728bb // indirect
)
