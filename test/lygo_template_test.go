package test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_template"
	"fmt"
	"testing"
)

func TestTemplate(t *testing.T) {

	model, err := lygo_json.ReadMapFromFile("./model.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	engine := lygo_ext_template.NewTemplateEngine()
	err = engine.OpenModel("./tpl.txt")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("Paragraphs", len(engine.Paragraphs()))

	errs := engine.Render(model)
	if len(errs) > 0 {
		t.Error(errs)
		t.FailNow()
	}
	fmt.Println(engine)
	engine.SaveTo("./doc.txt")
}
