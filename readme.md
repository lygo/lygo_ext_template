#lygo_template

Simple library to generate text file merged with data.

## How to Use

To use just call:

`go get -u bitbucket.org/lygo/lygo_ext_template`

## Dependencies

 `go get -u bitbucket.org/lygo/lygo_commons`

 `go get -u github.com/cbroglie/mustache`
 
 ### Versioning
 
 Sources are versioned using git tags:
 
 ```
 git tag v0.1.0
 git push origin v0.1.0
 ```
